#!/bin/bash

# PHP-FPM tuning
sed -i -e 's#listen\s*=\s*127.0.0.1:9000#listen = '$LISTEN'#g' \
	-e "s/pm.max_children\s*=\s*5/pm.max_children = $MAX_CHILDREN/g" \
	-e "s/pm.start_servers\s*=\s*2/pm.start_servers = $START_SERVER/g" \
	-e "s/pm.min_spare_servers\s*=\s*1/pm.min_spare_servers = $MIN_SPARE_SERVERS/g" \
	-e "s/pm.max_spare_servers\s*=\s*3/pm.max_spare_servers = $MAX_SPARE_SERVERS/g" \
	-e "s/;pm.max_requests\s*=\s*500/pm.max_requests = $MAX_REQUESTS/g" \
	-e "s/;listen.owner\s*=\s*www-data/listen.owner = root/g" \
	-e "s/;listen.group\s*=\s*www-data/listen.group = root/g" \
	-e "s/;listen.mode\s*=\s*www-data/listen.mode = 0660/g" \
	/usr/local/etc/php-fpm.d/www.conf

# php.ini
echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20151012/xdebug.so
xdebug.remote_enable=1
xdebug.remote_port = 9001
xdebug.remote_host = 192.168.1.185
xdebug.remote_connect_back = 1
upload_max_filesize = $MAX_UPLOAD_SIZE
date.timezone = Etc/UTC
upload_max_filesize = 40M
post_max_size = 40M
memory_limit = 2048M
post_max_size = $MAX_UPLOAD_SIZE
display_errors = Off
log_errors = On
error_log = /var/log/php-errors.log" > /usr/local/etc/php/php.ini


ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default

# Supervisord
supervisord -n
