L'esercizio consiste nello sviluppare una piccola applicazione in Slim microframework per gestire una lista di utenti e la loro autenticazione. Non potranno esistere utenti con la stessa email o con lo stesso username e la password dovrà essere lunga minimo 4 caratteri e contenere almeno una lettera maiuscola e un numero.

Usare composer per gestire le dipendenze e GIT come sistema di version control.

Andrà scritta una migrazione usando la libreria Phinx per creare la tabella dove verranno salavati gli utenti.

L'applicazione sarà composta da:

1) script da cli usato per importare il file utenti.csv. Lo script dovrà occuparsi di criptare la password prima di salvarla a DB perchè salvare le password in chiaro è una cosa molta brutta ed ogni volta che viene fatto un uccellino muore :)

2) layer di API in REST cosituito da 2 API. Una per fare il login e una per la ricerca di un utente.

L'API del login si aspetta nel body della richiesta un json contenente "username" e "password" dell'utente che si sta autenticando. In caso di successo ritorna il JWT che verrà usato per autenticare le richieste successive. In caso di errore ritorna lo status code più corretto per la tipologia di errore (es: 404 in caso di utente non trovato)

POST /login
	{
		"username": "XXXX",
		"password": "YYYYY"
	}


L'API della ricerca permette di ricercare gli utenti per nome, per cognome oppure per un intervallo di date di compleanno oppure per una combinazione di tutti i criteri precedenti (es: tutti gli utenti che si chiamano Pippo nati in un certo intervallo di date). La chiamata all'API potrà avvenire solo usando il JWT generato durante il login. In assenza del JWT dovrà tornare un errore con la status HTTP più idoneo. (403)

GET /users?firstName=XXX&lastName=YYYY&birthday=startDate|endDate


Note/Suggerimenti:

- Salvare la password rispettando le indicazioni per il password hashing di PHP

- Usare la libreria Doctrine DBAL per fare le query a DB.

- Se si vuole si possono strutturare le API usando le specifiche JSONAPI (http://jsonapi.org/)
