<?php

$dbname = getenv('MYSQL_DATABASE');
$user = getenv('MYSQL_USER');
$password = getenv('MYSQL_PASSWORD');
$host = getenv('MYSQL_HOST');

return [
    'type' => 'mysql',
    'options' => [
        'PDO::MYSQL_ATTR_INIT_COMMAND' => 'SET NAMES \'UTF8\''
    ],
    'dsn' => 'mysql:dbname=' . $dbname . ';host=' . $host,
    'host' => $host,
    'name' => $dbname,
    'user' => $user,
    'password' => $password,
];
