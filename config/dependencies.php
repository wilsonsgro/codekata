<?php

use Firebase\JWT\JWT;
use App\Manager\Manager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use App\Controller\UserController;
use App\DataAccessLayer\Bussiness\UserDAO;

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['doctrineconnection'] = function($c) {
    $settings = $c->get('settings')['connection'];
    $connection = DriverManager::getConnection(array(
        'dbname' => $settings['name'],
        'user' => $settings['user'],
        'password' => $settings['password'],
        'host' => $settings['host'],
        'driver' => 'pdo_mysql',
        'charset' => 'utf8',
        'driverOptions' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ), $config = new Configuration);
    return $connection;
};

$container['secret-key'] = getenv("JWT_SECRET");

$container['jwt'] = function($c) {
    return  new JWT();
};

$container['userdao'] = function($c) {
    return new UserDAO(
        $c->get('doctrineconnection')
    );
};

$container['manager'] = function($c) {
    return new Manager($c);
};

$container['UserController'] = function($c) {
    return new UserController(
        $c->get('manager')
    );
};
