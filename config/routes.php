<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
// login
$app->post("/login", 'UserController:login');
// middleware
$app->group(
    '/api', function () {
    $this->post('/insert', 'UserController:insertUser');
    $this->get('/users', 'UserController:searchUsers');
});
