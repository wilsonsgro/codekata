<?php
use Slim\Middleware\JwtAuthentication;

$app->add(new JwtAuthentication([
    "secret" => $container->get('secret-key'),
    "path" => "/api",
    "algorithm" => [ "HS256" ],
    "attribute" => "decoded_token_data",
    'secure' => false,
    "error" => function ($request, $response) {
    $data["status"] = "error";
    $data["message"] = 'Unauthorized';
    return $response
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
]));
