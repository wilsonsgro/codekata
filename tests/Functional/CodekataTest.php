<?php

namespace Tests\Functional;

use Slim\App;
use Faker\Factory;
use GuzzleHttp\Client;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Environment;
use GuzzleHttp\Exception\RequestException;

class CodekataTest extends \PHPUnit_Framework_TestCase
{
    var $url;
    var $hostname;
    const USERCORRECT = 'lorenzod';
    const PASSWORDCORRECT = '$fV1v!-_er';
    const TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VybmFtZSI6ImxvcmVuem9kIn0.sKsXpa8RRt1re1p-Lmo4JUVduavOvb4r9uAYBgk9ByQ';

    private function init()
    {
        $this->client = new Client();
        $this->hostname = 'local.codekata';
    }

    public function testLogin()
    {
        $this->init();
        $url = $this->hostname . '/login';
        $data = ['username' => CodekataTest::USERCORRECT ,
                'password' =>  CodekataTest::PASSWORDCORRECT];

        try {
                $response = $this->client->request('POST', $url , [
                     'headers' => ['Content-Type' => 'application/json'],
                     'body' => json_encode($data)]);

                $code = $response->getStatusCode();
                $this->assertEquals(200, $code);
                $contentType = $response->getHeaders()["Content-Type"][0];
                $this->assertEquals("application/json;charset=utf-8", $contentType);

        } catch (RequestException $e) {
            $response = $e->getResponse();
        }
    }

    public function testInsert()
    {
        $this->init();
        // password errata
        $password = "111";
        $url = $this->hostname . '/api/insert';
        $data = [
            'username' =>'damianop1sdas',
            'firstName' => 'Petrungaro1',
            'lastname' => 'Damiano',
            'email' => 'dp@qwentasdaes.it',
            'password' => $password,
            'birthday' => '1970-02-30'
        ];

        try {
            $token = CodekataTest::TOKEN;
            $response = $this->client->request('POST', $url , [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                        'Accept'        => 'application/json',
                    ],
                    'body' => json_encode($data)]
            );

            $code = $response->getStatusCode();
            $this->assertEquals(500, $code);

        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

    }

    public function testSearch()
    {
        $this->init();
        $url = $this->hostname . '/api/search';
        $data = [
            'username' =>'damianop1',
            'firstName' => 'Petrungaro1',
            'lastname' => 'Damiano',
            'email' => 'dp@qwentes.it',
            'password' => '12SDff$%',
            'birthday' => '1970-02-30'
        ];

        try {
            $token = CodekataTest::TOKEN;

            $response = $this->client->request('GET', $url , [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                        'Accept'        => 'application/json',
                    ],
                    'body' => json_encode($data)]
            );

            $code = $response->getStatusCode();
            $this->assertEquals(200, $code);
            $contentType = $response->getHeaders()["Content-Type"][0];
            $this->assertEquals("application/json;charset=utf-8", $contentType);

        } catch (RequestException $e) {
            $response = $e->getResponse();
        }
    }
}
// command shell
//  ./vendor/bin/phpunit ./tests/Functional/CodekataTest.php
// rember to add this:  nano /etc/hosts 127.0.0.1 local.codekata
