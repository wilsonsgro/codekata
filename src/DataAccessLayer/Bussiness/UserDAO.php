<?php

namespace App\DataAccessLayer\Bussiness;

use Doctrine\DBAL\DBALException;


class UserDAO
{
    /**
     * @var $connection
     */
    private $connection;

    /**
     * Initializes the object.
     *
     * @param Doctrine\DBAL\Connection $connection
     *
     */
    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get query builder.
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function getQueryBuilder()
    {
        return $this->connection->createQueryBuilder();
    }

    /**
     * @param string $username
     * @return [] user
     */
    public function getByUsername($username)
    {
        $queryBuilder = $this->getQueryBuilder();

        $user = $queryBuilder
            ->select('*')
            ->from('users')
            ->where('username = ?')
            ->setParameter(0, $username)
            ->execute()
            ->fetch();

        return $user;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $dateStart
     * @param string $dateEnd
     * @return  [] users
     */
    public function searchUsers($firstName, $lastName, $dateStart, $dateEnd)
    {
        $queryBuilder = $this->getQueryBuilder();

        try {
            $users = $queryBuilder
                ->select('*')
                ->from('users')
                ->where('firstName = ?')
                ->orWhere('lastName = ?')
                ->andWhere('birthday BETWEEN ? AND ?')
                ->setParameter(0, $firstName)
                ->setParameter(1, $lastName)
                ->setParameter(2, $dateStart)
                ->setParameter(3, $dateEnd)
                ->execute()
                ->fetchAll();
        } catch (DBALException $e) {
            $message =  'Caught exception: ' . $e->getMessage();
            return false;
        }
        return $users;
    }

    /**
     * @param string $user
     * @return [] user
     */
    public function insertUser($user)
    {
        $queryBuilder = $this->getQueryBuilder();

        try {
            $queryBuilder
                ->insert('users')
                ->values(
                    array(
                        'firstName' => '?',
                        'lastName' => '?',
                        'email' => '?',
                        'username' => '?',
                        'birthday' => '?',
                        'password' => '?'
                    )
                )
                ->setParameter(0, $user ['firstName'])
                ->setParameter(1,  $user['lastName'])
                ->setParameter(2,  $user['email'])
                ->setParameter(3,  $user['username'])
                ->setParameter(4,  '2018-09-21 00:00:00')
                ->setParameter(5,  $user['password']);
            $queryBuilder->execute();
        } catch (DBALException $e) {
            $message =  'Caught exception: ' . $e->getMessage();
            return false;
        }
        return $user;
    }

    /**
     *
     * @return [] users
     */
    public function fetchAll()
    {
        $queryBuilder = $this->getQueryBuilder();

        $queryBuilder
            ->select('*')
            ->from('users');
        return $queryBuilder
                    ->execute()
                    ->fetchAll();
    }
}
