<?php

namespace App\Controller;

use Firebase\JWT\JWT;
use Slim\Http\Request;
use Slim\Http\Response;

class UserController
{
    /**
     * @var $manager
     */
    protected $manager;

   /**
    * Initializes the object.
    *
    * @param App\Manager\Manager $manager
    *
    */
    public function __construct($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/login")
     * @Method({"POST"})
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return JsonResponse|\Slim\Http\Respons\Response
     */
    public function login(Request $request, Response $response, $args)
    {
        header('Content-Type: application/json');

        $post = $request->getParsedBody();
        $username = $post['username'];
        $password = $post['password'];
        $user = $this->manager
            ->getByUsernameAndPassword($username, $password);

        if (!$user) {
            return $response
                ->withStatus(302)
                ->withJson(['error' => true, 'message' => 'These credentials do not match our records.']);
        }

        $payload = [
            'id' => $user['id'],
            'username' => $user['username'],
            'datetime' => date("Y-m-d H:i:s")
        ];

        $token = $this->manager->createToken($payload);

        return $response
            ->withJson([
                'token' => $token
            ]);
    }

    /**
     * @Route("/api/insert")
     * @Method({"POST"})
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return JsonResponse|\Slim\Http\Respons\Response
     */
    public function insertUser(Request $request, Response $response, $args)
    {
        $post = $request->getParsedBody();
        //check regex password password
        $post['password'] = $this->manager->regexPassword($post['password']);

        if (!$post['password']) {
            return $response
                ->withStatus(500)
                ->withJson(['error' => true, 'message' => 'password in not correct']);
        }

        $data = $this->manager->insertUser($post);

        if (!$data) {
            return $response
                ->withStatus(500)
                ->withJson(['error' => true, 'message' => 'user is present into db']);
        }

        return $response->withJson([
            'message' => 'Insert into database',
            'data' => $data]
        );
    }

    /**
     * @Route("/api/users")
     * @Method({"GET"})
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return JsonResponse|\Slim\Http\Respons\Response
     */
    public function searchUsers(Request $request, Response $response, $args)
    {
        $parameters = $request->getQueryParams();

        $firstName = $parameters['firstName'];
        $lastName = $parameters['lastName'];
        $lastName = $parameters['birthday'];
        $rangeBirthday = explode("|", $parameters['birthday']);

        $dateStart = $rangeBirthday[0];
        $dateEnd = $rangeBirthday[1];

        $data = $this->manager->searchUsers($firstName,$lastName, $dateStart, $dateEnd);

        if (!$data) {
            return $response
                ->withStatus(500)
                ->withJson(['error' => true, 'message' => 'error for the search']);
        }

        return $response->withJson([
            'message' => 'OK',
            'data' => $data]
        );
    }
}
