<?php

namespace App\CustomPHPExcelObject;

class CustomPHPExcelObject
{
    /* Data types */
    const TYPE_STRING2  = 'str';
    const TYPE_STRING   = 's';
    const TYPE_FORMULA  = 'f';
    const TYPE_NUMERIC  = 'n';
    const TYPE_BOOL     = 'b';
    const TYPE_NULL     = 'null';
    const TYPE_INLINE   = 'inlineStr';
    const TYPE_ERROR    = 'e';

    /**
     * List of error codes
     *
     * @var array
     */
    private static $_errorCodes = array(
        '#NULL!'  => 0,
        '#DIV/0!' => 1,
        '#VALUE!' => 2,
        '#REF!'   => 3,
        '#NAME?'  => 4,
        '#NUM!'   => 5,
        '#N/A'    => 6
    );


    private $phpExcelIO;

    public function __construct($phpExcelIO = '\PHPExcel_IOFactory')
    {
        $this->phpExcelIO = $phpExcelIO;
    }

    /**
     * Creates an empty PHPExcel Object if the filename is empty, otherwise loads the file into the object.
     *
     * @param string $filename
     *
     * @return \PHPExcel
     */
    public function createPHPExcelObject($filename = null)
    {
        return (null === $filename) ? new \PHPExcel() : call_user_func(array($this->phpExcelIO, 'load'), $filename);
    }

    /**
     * Create a worksheet drawing
     * @return \PHPExcel_Worksheet_Drawing
     */
    public function createPHPExcelWorksheetDrawing()
    {
        return new \PHPExcel_Worksheet_Drawing();
    }

    /**
     * Create a writer given the PHPExcelObject and the type,
     *   the type coul be one of PHPExcel_IOFactory::$_autoResolveClasses
     *
     * @param \PHPExcel $phpExcelObject
     * @param string    $type
     *
     *
     * @return \PHPExcel_Writer_IWriter
     */
    public function createWriter(\PHPExcel $phpExcelObject, $type = 'Excel5')
    {
        return call_user_func(array($this->phpExcelIO, 'createWriter'), $phpExcelObject, $type);
    }


    /**
     * Create a PHPExcel Helper HTML Object
     *
     * @return \PHPExcel_Helper_HTML
     */
    public function createHelperHTML()
    {
        return new \PHPExcel_Helper_HTML();
    }

    public function columnIndexFromString($highestColumn)
    {
        return CustomPHPExcelObject::columnIndexFromStringStatic($highestColumn);
    }

    /**
     * Get list of error codes
     *
     * @return array
     */
    public static function getErrorCodes()
    {
        return self::$_errorCodes;
    }

    /**
     * DataType for value
     *
     * @param   mixed  $pValue
     * @return  string
     */
    public static function dataTypeForValue($pValue = null) {
        // Match the value against a few data types
        if ($pValue === null) {
            return CustomPHPExcelObject::TYPE_NULL;
        } elseif ($pValue === '') {
            return CustomPHPExcelObject::TYPE_STRING;
        } elseif ($pValue instanceof PHPExcel_RichText) {
            return CustomPHPExcelObject::TYPE_INLINE;
        } elseif ($pValue{0} === '=' && strlen($pValue) > 1) {
            return CustomPHPExcelObject::TYPE_FORMULA;
        } elseif (is_bool($pValue)) {
            return CustomPHPExcelObject::TYPE_BOOL;
        } elseif (is_float($pValue) || is_int($pValue)) {
            return CustomPHPExcelObject::TYPE_NUMERIC;
        } elseif (preg_match('/^[\+\-]?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)([Ee][\-\+]?[0-2]?\d{1,3})?$/', $pValue)) {
            $tValue = ltrim($pValue, '+-');
            if (is_string($pValue) && $tValue{0} === '0' && strlen($tValue) > 1 && $tValue{1} !== '.' ) {
                return CustomPHPExcelObject::TYPE_STRING;
            } elseif((strpos($pValue, '.') === false) && ($pValue > PHP_INT_MAX)) {
                return CustomPHPExcelObject::TYPE_STRING;
            }
            return CustomPHPExcelObject::TYPE_NUMERIC;
        } elseif (is_string($pValue) && array_key_exists($pValue, CustomPHPExcelObject::getErrorCodes())) {
            return CustomPHPExcelObject::TYPE_ERROR;
        }

        return CustomPHPExcelObject::TYPE_STRING;
    }

    public static function columnIndexFromStringStatic($pString = 'A')
    {
        //	Using a lookup cache adds a slight memory overhead, but boosts speed
        //	caching using a static within the method is faster than a class static,
        //		though it's additional memory overhead
        static $_indexCache = array();

        if (isset($_indexCache[$pString]))
            return $_indexCache[$pString];

        //	It's surprising how costly the strtoupper() and ord() calls actually are, so we use a lookup array rather than use ord()
        //		and make it case insensitive to get rid of the strtoupper() as well. Because it's a static, there's no significant
        //		memory overhead either
        static $_columnLookup = array(
            'A' => 1, 'B' => 2, 'C' => 3, 'D' => 4, 'E' => 5, 'F' => 6, 'G' => 7, 'H' => 8, 'I' => 9, 'J' => 10, 'K' => 11, 'L' => 12, 'M' => 13,
            'N' => 14, 'O' => 15, 'P' => 16, 'Q' => 17, 'R' => 18, 'S' => 19, 'T' => 20, 'U' => 21, 'V' => 22, 'W' => 23, 'X' => 24, 'Y' => 25, 'Z' => 26,
            'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5, 'f' => 6, 'g' => 7, 'h' => 8, 'i' => 9, 'j' => 10, 'k' => 11, 'l' => 12, 'm' => 13,
            'n' => 14, 'o' => 15, 'p' => 16, 'q' => 17, 'r' => 18, 's' => 19, 't' => 20, 'u' => 21, 'v' => 22, 'w' => 23, 'x' => 24, 'y' => 25, 'z' => 26
        );

        //	We also use the language construct isset() rather than the more costly strlen() function to match the length of $pString
        //		for improved performance
        if (isset($pString{0})) {
            if (!isset($pString{1})) {
                $_indexCache[$pString] = $_columnLookup[$pString];
                return $_indexCache[$pString];
            } elseif(!isset($pString{2})) {
                $_indexCache[$pString] = $_columnLookup[$pString{0}] * 26 + $_columnLookup[$pString{1}];
                return $_indexCache[$pString];
            } elseif(!isset($pString{3})) {
                $_indexCache[$pString] = $_columnLookup[$pString{0}] * 676 + $_columnLookup[$pString{1}] * 26 + $_columnLookup[$pString{2}];
                return $_indexCache[$pString];
            }
        }
        ///throw new PHPExcel_Exception("Column string index can not be " . ((isset($pString{0})) ? "longer than 3 characters" : "empty"));
    }
}
