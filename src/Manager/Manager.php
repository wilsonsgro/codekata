<?php

namespace App\Manager;

use Firebase\JWT\JWT;

class Manager
{
    /**
     * @var $container
     */
    protected $container;

   /**
    * Initializes the object.
    *
    * @param Slim\Container $container
    *
    */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @return Slim\Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return Doctrine\DBAL\Connection
     */
    public function getConnection()
    {
        return $this->container->get('doctrineconnection');
    }

    /**
     * @return Firebase\JWT\JWT
     */
    public function getJwt()
    {
        return $this->container->get('jwt');
    }

    /**
     * @param string $username
     * @param string $password
     * @return  [] user
     */
    public function getByUsernameAndPassword($username, $password)
    {
        $userdao = $this->container->get('userdao');
        $user = $userdao->getByUsername($username);

        if (!password_verify($password, $user['password'])) {
            return false;
        } else {
            return $user;
        }
    }

    /**
     * @param string $username
     * @param string $password
     * @return  [] user
     */
    public function insertUser($user)
    {
        $userdao = $this->container->get('userdao');
        $user['password'] = $this->generatePasswordHash($user['password']);
        $user = $userdao->insertUser($user);
        if (!$user) return false;
        return ['user' => $user];
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $dateStart
     * @param string $dateEnd
     * @return  [] users
     */
    public function searchUsers($firstName, $lastName, $dateStart, $dateEnd)
    {
        $userdao = $this->container->get('userdao');
        $users = $userdao->searchUsers($firstName, $lastName, $dateStart, $dateEnd);
        if (!$users) return false;
        return ['users' => $users];
    }

    /**
     * @param string $passowrd
     * @return string password_hash
     */
    public function generatePasswordHash($passowrd)
    {
        $options = [
            'cost' => 10
        ];

        return password_hash($passowrd, PASSWORD_BCRYPT, $options);
    }

    /**
     * @param string $passowrd
     * @return string password_hash
     */
    public function regexPassword($password)
    {
        $pattern = $this->container->get('settings')['patternregex'];
        if(preg_match($pattern, $password)){
            return $password;
        } else{
            return false;
        }
    }

    /**
     * @param $payload []
     * @return string A signed JWT
     */
    public function createToken($payload)
    {
         $setting = $this->container->get('settings');
        return JWT::encode($payload, $setting['jwt']['secret'], "HS256");
    }
}
