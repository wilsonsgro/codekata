<?php

namespace Command;

use App\CustomPHPExcelObject\CustomPHPExcelObject;

class ImportCsv
{
    /**
     * @var const path
     */
    const  FILE_UTENTI = __DIR__ . '/../'. 'utenti.csv';

    /**
     * @var $arrUsers
     */
    private $arrUsers =  [] ;

    /**
     * @var $manager
     */
    protected $manager;

   /**
    * Initializes the object.
    *
    * @param App\Manager\Manager $manager
    *
    */
    public function __construct($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return [] $arrUsers
     */
    public function getUsersFromCsv()
    {
        $customPHPExcelObject = new CustomPHPExcelObject();
        $objPHPExcel = $customPHPExcelObject->createPHPExcelObject(ImportCsv::FILE_UTENTI);

        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $highestRow         = $worksheet->getHighestRow();
            $highestColumn      = $worksheet->getHighestColumn();
            $highestColumnIndex = $customPHPExcelObject->columnIndexFromString($highestColumn);

            for ($row = 2; $row <= $highestRow; ++ $row) {
                $user = [];
                for ($col = 0; $col < $highestColumnIndex; ++ $col) {
                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                    $val = $cell->getValue();

                    switch ($col) {
                        case 0:
                            $user['firstName'] = $cell->getValue();
                        break;
                        case 1:
                            $user['lastName'] = $cell->getValue();
                        break;
                        case 2:
                            $user['email'] = $cell->getValue();
                        break;
                        case 3:
                            $user['username'] = $cell->getValue();
                        break;
                        case 4:
                            $user['password'] = $cell->getValue();
                        break;
                    }
                }
                $this->arrUsers [] = $user;
            }
        }

        return $this->arrUsers;
    }

    /**
     *
     */
    public function insertUsers()
    {
        $arryUser = $this->getUsersFromCsv();
        $conn = $this->manager->getConnection();
        $queryBuilder = $conn->createQueryBuilder();

        foreach ($arryUser as $user) {

            $password = $this->manager->generatePasswordHash($user['password']);

            try {
                $queryBuilder
                    ->insert('users')
                    ->values(
                        array(
                            'firstName' => '?',
                            'lastName' => '?',
                            'email' => '?',
                            'username' => '?',
                            'birthday' => '?',
                            'password' => '?'
                        )
                    )
                    ->setParameter(0, $user ['firstName'])
                    ->setParameter(1,  $user['lastName'])
                    ->setParameter(2,  $user['email'])
                    ->setParameter(3,  $user['username'])
                    ->setParameter(4,  '2018-09-21 00:00:00')
                    ->setParameter(5,  $password);
                $queryBuilder->execute();
            } catch (\Exception $e) {
                $message =  'Caught exception: ' . $e->getMessage();
            }
        }
    }
}
