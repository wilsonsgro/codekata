<?php

require __DIR__ . '/../vendor/autoload.php';

use Command\ImportCsv;

if (PHP_SAPI == 'cli') {

    $argv = $GLOBALS['argv'];
    array_shift($argv);
    $pathInfo = implode('/', $argv);
    $env = \Slim\Http\Environment::mock(['REQUEST_URI' => $pathInfo]);

    $dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
    $dotenv->load();
    // Instantiate the app
    $settings = require __DIR__ . '/../config/settings.php';
    $app = new \Slim\App($settings);

    $settings['environment'] = $env;
    $app = new \Slim\App($settings);
    $container = $app->getContainer();

    // Set up dependencies
    require __DIR__ . '/../config/dependencies.php';

    $container['errorHandler'] = function ($container) {
        return function ($request, $response, $exception) use ($container) {
            echo 'error' . "\n";
            exit(1);
        };
    };
    $container['notFoundHandler'] = function ($container) {
        return function ($request, $response) use ($container) {
            echo 'not_found' . "\n";
            exit(1);
        };
    };

    $app->get('/importcsv', function ($request, $response, $args)  use ($container) {

        echo 'Start command' . "\n";
        $manager  = $container->get('manager');
        $importCsv = new ImportCsv($manager);
        $importCsv->insertUsers();
        echo 'End command' . "\n";

    });

    $app->run();
}