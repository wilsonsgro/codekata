Codekata
 -----------

### Requirements

![Docker >= 17.04 ](https://badgen.net/badge/Docker/>=17.04/409be6?icon=docker)

![docker-compose >=1.8.0 ](https://badgen.net/badge/docker-compose/>=1.8/409be6?icon=docker)
 
### Setup

```bash
$ cp .env.dist .env
```

### Build

```bash
$ docker-compose up -d
```

### Composer install

```bash
$ docker-compose exec codekata composer install
```
if have problem with your memory RAM use this
```bash
$ docker-compose exec codekata php -d memory_limit=1 composer.phar install
```

#### dns local 

```bash
$ sudo nano /etc/hosts 127.0.0.1 local.codekata
```

### Commands to enter containers

```bash
$ docker-compose exec codekata bash
$ docker-compose exec mariadb bash
```
###  Migrate Phinx
```bash
$ docker-compose exec codekata php composer.phar require robmorgan/phinx
$ docker-compose exec php php composer.phar install --no-dev
$ docker-compose exec codekata vendor/bin/phinx init .
$ $EDITOR phinx.yml
$ docker-compose exec codekata mkdir -p db/migrations db/seeds
$ docker-compose exec codekata vendor/bin/phinx create CodeKataMigration
$ docker-compose exec codekata vendor/bin/phinx migrate -e development
```
### Commands to import file csv

```bash
$ docker-compose exec codekata php command/cli.php /importcsv
```

#### Hey oh Let's Go

[local.codekata](http://local.codekata)

### Tests Functional

```bash
$ docker-compose exec codekata  ./vendor/bin/phpunit ./tests/Functional/CodekataTest.php
```
with docker-machine
```bash
$ docker-compose exec codekata  nano /etc/hosts 127.0.0.1 local.codekata
```

### Tests Ping

```bash
$ docker-compose exec codekata  ping local.codekata
```

### Istall Slim Framework
```bash
$ docker-compose exec codekata php composer.phar create-project slim/slim-skeleton codekata
```

###  After create new file php
```bash
docker-compose exec codekata php composer.phar  dump-autoload -o
```

## Teach
> [Slim Framework](https://www.slimframework.com/)

> [Slim Framework - Environment variables using dotenv](https://akrabat.com/configuration-in-slim-framework/)

> [Slim framework - Doctrine dbal](https://www.codementor.io/lautiamkok/using-eloquent-doctrine-dbal-or-medoo-with-slim-3-byr7kyj59)

> [Slim Framework - Jwt-auth ](https://github.com/tuupola/slim-jwt-auth)

> [Slim Framework - Command cli](https://gist.github.com/jmas/0ed24fe00bc2a8eb8361431e55d0cda8)

> [jwt-slim3-framework](https://arjunphp.com/secure-web-services-using-jwt-slim3-framework/)
> [Php code checker](https://phpcodechecker.com/)

> [password_verify issues](https://www.experts-exchange.com/questions/28966548/password-verify-issues.html)

> [Html beautifier](https://www.freeformatter.com/html-formatter.html#ad-output)

> [Javascript beautifier](https://beautifier.io/)

> [regex101 ](https://regex101.com/)

> [Tutorial php regex ](https://www.tutorialrepublic.com/php-tutorial/php-regular-expressions.php)

> [Check password](http://www.rubular.com/r/hpDwhMCTCX)

> [JSONAPI](http://jsonapi.org/)]

## Curiosity

> [Qual è la parola più lunga della lingua italiana](https://www.focus.it/cultura/curiosita/qual-e-la-parola-piu-lunga-della-lingua-italiana)
